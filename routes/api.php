<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::resource("/products","ProductController");

Route::post("/products/import","ProductController@Import");

Route::get("/user/list/export","ImportController@csv_export");

Route::get("/country","ProductController@country");

Route::group(['middleware' => 'auth:api'], function(){
Route::post('/details', 'API\UserController@details');
Route::post('/userdetails','API\UserController@UserDetails');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
