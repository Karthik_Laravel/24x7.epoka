<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/{test}', function () {
    return view('welcome');
});

Route::get('/mail/sendemail', 'SendEmailController@index');
Route::post('/mail/sendemail/send', 'SendEmailController@send');

Route::get('/file/upload', 'PagesController@index'); // localhost:8000/
Route::post('/file/upload', 'PagesController@uploadFile');

Route::resource("/user/list/","ImportController");

Route::get("/user/list/export","ImportController@csv_export");

Route::post('/import/csv_file/import', 'CsvFile@csv_import')->name('import');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
