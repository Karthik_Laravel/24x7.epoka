<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
//  ["cust_id","user_id","DateUpdated","Ref","Model","Serial_Num",

// "Order_Type","audit_status","Service_Level","Ship_FSL","Notes",

// "Inv_Quantity","Inv_Partdesc","Inv_ProdClass"];

    public function up()
    {
        Schema::create('audit_log', function (Blueprint $table) {
            $table->id();
            $table->string("cust_id");
            $table->string("user_id");
            $table->string("Ref");
            $table->string("Model");
            $table->string("Serial_Num");
            $table->string("Order_Type");
            $table->string("audit_status");
            $table->string("Service_Level");
            $table->string("Ship_FSL");
            $table->string("Notes");
            $table->string("Inv_Quantity");
            $table->string("Inv_Partdesc");
            $table->string("Inv_ProdClass");
            $table->timestamp("DateUpdated");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_logs');
    }
}
