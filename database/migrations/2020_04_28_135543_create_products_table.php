<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // ["Id","cust_id","Sup_Location","Address","MFG","Model","Serial_Num","SLA","Part_Location","audit_status","price","from_date","to_date","status"]
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer("cust_id");
            $table->String("Sup_Location");
            $table->text("Address");
            $table->String("MFG");
            $table->String("Model");
            $table->String("Serial_Num");
            $table->String("SLA");
            $table->String("Part_Location");
            $table->String("audit_status");
            $table->String("price");
            $table->string("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
