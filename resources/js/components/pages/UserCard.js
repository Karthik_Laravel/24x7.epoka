import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

export default function ColorTextFields() {
  const classes = useStyles();

  return (
   
    <div>
      <form noValidate autoComplete="off">
        <Grid container item xs={12}>
        <Grid container item  spacing={3}>
          <Grid item xs={3}>
            <TextField
            id="outlined-secondary"
            label="FIRSTNAME"
            variant="outlined"
            color="secondary"
            fullWidth
          />
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="LASTNAME"
          variant="outlined"
          color="secondary"
          fullWidth
          />
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="USERNAME"
          variant="outlined"
          color="secondary"
          fullWidth
          />
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="CURRENCY"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="STATUS"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="ROLES"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="MENU FILTERS"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="CONTACT PHONE"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="PHONE"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="BUILDING"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="ADDRESS"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="CITY"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="ZIP"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="COUNTRY"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="COMPANY"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="CUSTOMERS"
          variant="outlined"
          color="secondary"
          fullWidth
          /> 
          </Grid>
        </Grid>
        </Grid> 
        
     </form>
    </div>
    
    
     
  
  );
}
