import React, { useState,useEffect } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase'

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

export default function ColorTextFields() {
  const classes = useStyles();
  const [firstname,setFirstname] = useState('');
  const [lastname,setLastname] = useState('');
  const [username,setUsername] = useState('');
  const [email,setEmail] = useState('');
  const [password,setPassword] = useState('');
  const [currency,setCurrency] = useState('');
  const [Address,setAddress] = useState('');
  const [country,setCountry] = useState('');
  const [city,setCity] = useState('');
  const [Building,setBuilding] = useState('');
  const [zip,setZip] = useState('');
  const [menu_filters,setMenu_filters] = useState('');
  const [contactphone,setContactphone] = useState('');
  const [phone,setPhone] = useState('');
  const [customers,setCustomers] = useState('');
  const [company,setCompany] = useState('');
  const [status,setStatus] = useState('');
  const [roles,setRoles] = useState('');
  const [open, setOpen] = useState(false);
  const [countries,setCountries] = useState([]);

  function handleSubmit(e)
  {
    e.preventDefault();

    let data = {
      firstname:firstname,
      lastname:lastname,
      username:username,
      password:password,
      email:email,
      currency:currency,
      ContactPhone:contactphone,
      Phone:phone,
      customers:customers,
      Company:company,
      Address:Address,
      Building:Building,
      City:city,
      Country:country,
      Zip:zip,
      menu_filters:menu_filters,
      status:status,
      roles:roles


    }
    fetch("http://localhost:8000/api/register",{
      method:"post",
      headers:{
        "content-type":"application/json"
      },
      body:JSON.stringify(data)
    })
    .then((response) => response.json())
    .then((res) => 
    {
      setOpen(true);
      setFirstname('');
      setLastname('');
      setUsername('');

    }
    );
  }

  useEffect(() => {
    fetch("http://localhost:8000/api/country")
    .then(res => res.json())
      .then(response => {
        setCountries(response);
    })
  })
  return (

    
    
    <div>
     
        <Grid container item xs={12}>
        <Grid item xs={12}>
        <Collapse in={open}>
        <Alert
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
        >
         User Created Successfully !
        </Alert>
       </Collapse>
        <Typography variant="h6" color="primary" display="block" align="center" gutterBottom>
        CREATE NEW USER
        </Typography>
        </Grid>
        <form noValidate autoComplete="off" onSubmit={e => handleSubmit(e)}>
        <Grid container item  spacing={3}>
          <Grid item xs={3}>
            <TextField
            id="outlined-secondary"
            label="FIRSTNAME"
            variant="outlined"
            color="secondary"
            fullWidth
            onChange={e => setFirstname(e.target.value)}
          />
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="LASTNAME"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setLastname(e.target.value)}
          />
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="USERNAME"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setUsername(e.target.value)}
          />
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="Email"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setEmail(e.target.value)}
          />
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="PASSWORD"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setPassword(e.target.value)}
          />  
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="CURRENCY"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setCurrency(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={3}>
          <Select labelId="label" id="outlined-secondary" variant="outlined" color="secondary"
           value={status} onChange={e => setStatus(e.target.value)} style={{ background:"#fff" }} fullWidth>
            <MenuItem value="1">ACTIVE</MenuItem>
            <MenuItem value="0">INACTIVE</MenuItem>
          </Select>
          </Grid>
          <Grid item xs={3}>
          <Select labelId="label" id="outlined-secondary" variant="outlined" color="secondary"
           value={roles} onChange={e => setRoles(e.target.value)} style={{ background:"#fff" }} fullWidth>
            <MenuItem value="1">Admin</MenuItem>
            <MenuItem value="2">Account Manager</MenuItem>
            <MenuItem value="3">Customers</MenuItem>
          </Select>
          </Grid>
          <Grid item xs={3}>
          <Select labelId="label" id="outlined-secondary" variant="outlined" color="secondary"
           value={menu_filters} onChange={e => setMenu_filters(e.target.value)} style={{ background:"#fff" }} fullWidth>
            <MenuItem value="1">All Access</MenuItem>
            <MenuItem value="2">Products Access</MenuItem>
            <MenuItem value="3">User Access</MenuItem>
          </Select>
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="CONTACT PHONE"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setContactphone(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="PHONE"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setPhone(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="BUILDING"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setBuilding(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="ADDRESS"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setAddress(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="CITY"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setCity(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="ZIP"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setZip(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={3}>
          <Select labelId="label" id="outlined-secondary" variant="outlined" color="secondary"
          value={country} onChange={e => setCountry(e.target.value)} style={{ background:"#fff" }} fullWidth>
          {countries.map((value) => {
           return <MenuItem key={value.Name} value={value.Name}>{value.Name}</MenuItem>
          })};
          </Select>
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="COMPANY"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setCompany(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="outlined-secondary"
          label="CUSTOMERS"
          variant="outlined"
          color="secondary"
          fullWidth
          onChange={e => setCustomers(e.target.value)}
          /> 
          </Grid>
          <Grid item xs={12}>
          <Button variant="contained" align="right" color="primary" type="submit">ADD USER</Button>
          </Grid>
        </Grid>
        </form>
        </Grid> 

    
    </div>
    
    
     
  
  );
}
