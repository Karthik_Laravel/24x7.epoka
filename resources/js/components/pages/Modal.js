import React,{useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
  } from '@material-ui/pickers';
  import dateFormat from 'dateformat';
import ImportExport from './ImportExport';

export default function FormDialog() {
  const [open, setOpen] = React.useState(false);

  const [custom_id,setCustom_id] = useState("");
  const [sup_location,setSup_location] = useState("");
  const [address,setAddress] = useState("");
  const [mfg,setMFG] = useState("");
  const [model,setModel] = useState("");
  const [serial_no,setSerial_no] = useState("");
  const [sla,setSLA] = useState("");
  const [part_location,setPart_location] = useState("");
  const [audit_status,setAudit_status] = useState("");
  const [price,setPrice] = useState("");
  const [from_date,setFrom_date] = useState(new Date());
  const [to_date,setTo_date] = useState(new Date());

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit= (event) =>
  {
    
   
    event.preventDefault();

    let data = {
        cust_id:custom_id,
        Sup_Location:sup_location,
        Address:address,
        MFG:mfg,
        Model:model,
        Serial_Num:serial_no,
        SLA:sla,
        Part_Location:part_location,
        audit_status:audit_status,
        price:price,
        from_date:dateFormat(from_date,"yyyy-mm-dd"),
        to_date:dateFormat(to_date,"yyyy-mm-dd"),
        status:0
    }
    

    fetch("http://localhost:8000/api/products",
    {
        method:"post",
        headers:{
            "content-type":"application/json"
    },
    body:JSON.stringify(data),
    })
    .then((response) => console.log("response"))
    .then((res) => console.log(res))
    setOpen(false);
    
  }

  const [selectedDate, setSelectedDate] = React.useState(new Date());

  const handleFromDate = (date) => {
    setFrom_date(date);
  };

  const handleToDate = (date) => {
    setTo_date(date);
  };

  

  return (

   
    <div>
     <Grid container direction="row" justify="flex-end" alignItems="flex-start">
       <Fab size="small"  title="ADD PRODUCTS"  color="secondary" aria-label="add" onClick={handleClickOpen}>
         <AddIcon />
        </Fab>
        <ImportExport></ImportExport>
     </Grid>
     
      
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">ADD PRODUCT DETAILS</DialogTitle>
        <DialogContent>
          {/* <DialogContentText></DialogContentText> */}
          <form noValidate autoComplete="off" onSubmit={handleSubmit} >
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="Customer ID"
                    type="text"
                    onChange={e => setCustom_id(e.target.value)}
                   />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="Sup Location"
                    type="text"
                    onChange={e => setSup_location(e.target.value)}
                   />
                </Grid>
                <Grid item  xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="Address"
                    type="text"
                    onChange={e => setAddress(e.target.value)}
                   />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="MFG"
                    type="text"
                    onChange={e => setMFG(e.target.value)}
                   />
                </Grid>
                <Grid  item xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="Model"
                    type="text"
                    onChange={e => setModel(e.target.value)}
                   />
                </Grid>
                
                <Grid item  xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="SERIAL No"
                    type="text"
                    onChange={e => setSerial_no(e.target.value)}
                   />
                </Grid>
                <Grid item  xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="SLA"
                    type="text"
                    onChange={e => setSLA(e.target.value)}
                   />
                </Grid>
                <Grid item  xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="Part Location"
                    type="text"
                    onChange={e => setPart_location(e.target.value)}
                   />
                </Grid>
                <Grid  item xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="Audit Status"
                    type="text"
                    onChange={e => setAudit_status(e.target.value)}
                   />
                </Grid>
                <Grid item  xs={6}>
                    <TextField
                    margin="dense"
                    id="name"
                    label="Price"
                    type="text"
                    onChange={e => setPrice(e.target.value)}
                   />
                </Grid>
                <Grid item  xs={6}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="FROM DATE"
                    format="MM/dd/yyyy"
                    value={from_date}
                    onChange={handleFromDate}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item  xs={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="TO DATE"
                    format="MM/dd/yyyy"
                    value={to_date}
                    onChange={handleToDate}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
          </Grid>
          <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button color="primary" type="submit" >
            SUBMIT
          </Button>
        </DialogActions>
          </form>
        </DialogContent>
        
      </Dialog>
    </div>
  );
}
