import React, { Component } from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import 'date-fns';
import InputLabel from '@material-ui/core/InputLabel';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
  } from '@material-ui/pickers';
import dateFormat from 'dateformat';
import Modal from './Modal';
import cookies from 'react-cookies';
import Pagination from '@material-ui/lab/Pagination';

class Products extends Component {
    

    constructor(props) {
        super(props);
        this.state = { 
            Products:[],
            open:false,
            from_date:new Date(),
            to_date:new Date(),
            cust_id:"",
            Sup_Location:"",
            Address:"",
            MFG:"",
            Model:"",
            Serial_Num:"",
            SLA:"",
            Part_Location:"",
            audit_status:"",
            price:"",
            id:"",
            response:[],
            first_page:"",
            last_page:"",
            count:"",
         }
    this.handleDelete = this.handleDelete.bind(this);
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleFromDate = this.handleFromDate.bind(this);
    this.handleToDate = this.handleToDate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOnchange = this.handleOnchange.bind(this);
    }

    handleOnchange(e)
    {
        console.log(this.state);
    }
    
    handleClickOpen(id,cust_id,Sup_Location,Address,MFG,Model,Serial_Num,SLA,Part_Location,audit_status,price,from_date,to_date)
    {
        
        this.setState({cust_id:cust_id});
        this.setState({Sup_Location:Sup_Location});
        this.setState({Address:Address});
        this.setState({MFG:MFG});
        this.setState({Model:Model});
        this.setState({Serial_Num:Serial_Num});
        this.setState({SLA:SLA});
        this.setState({Part_Location:Part_Location});
        this.setState({audit_status:audit_status});
        this.setState({price:price});
        this.setState({from_date:from_date});
        this.setState({to_date:to_date});
        this.setState({id:id});

        this.setState({open:true});
    }

    handleClose()
    {
        this.setState({open:false});
    }
    handleFromDate(e)
    {
        this.setState({from_date:e});
    }
    handleToDate(e)
    {
        this.setState({to_date:e})
    }
    handleSubmit(e)
    {
        e.preventDefault();

       let id = this.state.id;
       let data={
           cust_id:this.state.cust_id,
           Sup_Location:this.state.Sup_Location,
           Address:this.state.Address,
           SLA:this.state.SLA,
           Model:this.state.Model,
           MFG:this.state.MFG,
           audit_status:this.state.audit_status,
           price:this.state.price,
           Serial_Num:this.state.Serial_Num,
           Part_Location:this.state.Part_Location,
           from_date:dateFormat(this.state.from_date,"yyyy-mm-dd"),
           to_date:dateFormat(this.state.to_date,"yyyy-mm-dd")

       }

    //    console.log(data);
        
        fetch('http://localhost:8000/api/products/' + id, {
            method:"put",
            headers:{
                "content-type":"application/json"
        },
        body:JSON.stringify(data),
        })
        .then((response) => response.json())
        .then((res) => console.log(res))

        this.setState({open:false});

    }

    componentDidMount()
    {
       
        fetch("http://localhost:8000/api/products")
        .then((res) => res.json())
        .then((response) => {
            console.log(response)
            this.setState({Products:response.data})
        })
    }

    handleDelete(id)
    {
        fetch('http://localhost:8000/api/products/' + id, {
        method: 'DELETE',
        })
        .then(res => res.json()) // or res.json()
        .then(res => console.log(res))
        window.location.reload();
    }

    render() { 
        

       
        return(
           <div>   
               <Modal></Modal>
               <TableContainer>
               <Table className="table" aria-label="custom pagination table">
                   <TableHead>
                       <TableRow>
                           {/* <TableCell><Checkbox /></TableCell> */}
                           <TableCell>Customer ID</TableCell>
                           <TableCell>Sup Location</TableCell>
                           <TableCell>Address</TableCell>
                           <TableCell>MFG</TableCell>
                           <TableCell>Model</TableCell>
                           <TableCell>Serial Num</TableCell>
                           <TableCell>SLA</TableCell>
                           <TableCell>Part Location</TableCell>
                           <TableCell>Audit Status</TableCell>
                           <TableCell>Price</TableCell>
                           <TableCell>From Date</TableCell>
                           <TableCell>To Date</TableCell>
                           <TableCell>Actions</TableCell>
                       </TableRow>
                   </TableHead>
                   <TableBody>
                    {this.state.Products.map((list) => {
                    return(
                        
                        <TableRow key={list.id}>
                        {/* <TableCell><Checkbox /></TableCell> */}
                        <TableCell component="th" scope="row">{list.cust_id}</TableCell>
                        <TableCell align="right">{list.Sup_Location}</TableCell>
                        <TableCell align="right">{list.Address}</TableCell>
                        <TableCell align="right">{list.MFG}</TableCell>
                        <TableCell align="right">{list.Model}</TableCell>
                        <TableCell align="right">{list.Serial_Num}</TableCell>
                        <TableCell align="right">{list.SLA}</TableCell>
                        <TableCell align="right">{list.Part_Location}</TableCell>
                        <TableCell align="right">{list.audit_status}</TableCell>
                        <TableCell align="right">{list.price}</TableCell>
                        <TableCell align="right">{list.from_date}</TableCell>
                        <TableCell align="right">{list.to_date}</TableCell>
                        <TableCell><EditIcon color="primary" 
                        onClick={() => this.handleClickOpen(list.id,list.cust_id,list.Sup_Location,list.Address,list.MFG,list.Model,list.Serial_Num,list.SLA,
                        list.Part_Location,list.audit_status,list.price,list.from_date,list.to_date)}>
                            </EditIcon> <DeleteIcon onClick={() => this.handleDelete(list.id)} color="error"></DeleteIcon> </TableCell>
                        </TableRow>
                        )
                    })
                }

                   </TableBody>
                </Table>
               </TableContainer>
               <Pagination count={10} color="primary" />
               <Dialog open={this.state.open} onClose={() => this.handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">EDIT PRODUCT DETAILS</DialogTitle>
                <DialogContent>
                <form noValidate autoComplete="off" onSubmit={(e) => this.handleSubmit(e)}>
                <Grid container>
                <Grid item xs={6}>
                <InputLabel htmlFor="component-simple">Customer ID</InputLabel>
                    <TextField
                    margin="dense"
                    id="standard-multiline-flexible"
                    multiline
                    autoFocus
                    type="text"
                    name="cust_id"
                    value={this.state.cust_id}
                    // onChange={() => this.handleOnchange}
                    onChange={e => this.setState({cust_id:e.target.value})}
                   />
                </Grid>
                <Grid item xs={6}>
                <InputLabel htmlFor="component-simple">Sup Location</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="Sup_Location"
                    value={this.state.Sup_Location}
                    onChange={e => this.setState({Sup_Location:e.target.value})}
                   />
                </Grid>
                <Grid item  xs={6}>
                <InputLabel htmlFor="component-simple">Address</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="Address"
                    value={this.state.Address}
                    onChange={e => this.setState({Address:(e.target.value)}) }
                   />
                </Grid>
                <Grid item xs={6}>
                <InputLabel htmlFor="component-simple">MFG</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="MFG"
                    value={this.state.MFG}
                    onChange={e => this.setState({MFG:(e.target.value)})}
                   />
                </Grid>
                <Grid  item xs={6}>
                <InputLabel htmlFor="component-simple">Model</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="Model"
                    value={this.state.Model}
                    onChange={e => this.setState({Model:(e.target.value)}) }
                   />
                </Grid>
                
                <Grid item  xs={6}>
                <InputLabel htmlFor="component-simple">SERIAL No</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="Serial_Num"
                    value={this.state.Serial_Num}
                    onChange={e => this.setState({Serial_no:(e.target.value)}) }
                   />
                </Grid>
                <Grid item  xs={6}>
                <InputLabel htmlFor="component-simple">SLA</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="SLA"
                    value={this.state.SLA}
                    onChange={e => this.setState({SLA:(e.target.value)})}
                   />
                </Grid>
                <Grid item  xs={6}>
                <InputLabel htmlFor="component-simple">Part Location</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="Part_Location"
                    value={this.state.Part_Location}
                    onChange={e => this.setState({Part_Location:(e.target.value)}) }
                   />
                </Grid>
                <Grid  item xs={6}>
                <InputLabel htmlFor="component-simple">Audit Status</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="audit_status"
                    value={this.state.audit_status}
                    onChange={e => this.setState({audit_status:(e.target.value)})}
                   />
                </Grid>
                <Grid item  xs={6}>
                <InputLabel htmlFor="component-simple">Price</InputLabel>
                    <TextField
                    margin="dense"
                     id="standard-multiline-flexible"
                    type="text"
                    name="price"
                    value={this.state.price}
                    onChange={e => this.setState({price:e.target.value}) }
                   />
                </Grid>
                <Grid item  xs={6}>
                <InputLabel htmlFor="component-simple">From Date</InputLabel>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="FROM DATE"
                    format="MM/dd/yyyy"
                    name="from_date"
                    value={this.state.from_date}
                    onChange={(e) => this.handleFromDate(e)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item  xs={6}>
                <InputLabel htmlFor="component-simple">To Date</InputLabel>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="TO DATE"
                    format="yyyy/MM/dd"
                    name="to_date"
                    value={this.state.to_date}
                    onChange={(e) => this.handleToDate(e)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                    
                </Grid>
                <DialogActions>
                <Button onClick={() => this.handleClose()} color="primary">
                    Cancel
                </Button>
                <Button color="primary" type="submit" >
                    SUBMIT
                </Button>
                </DialogActions>
                </form>

              </DialogContent>
            </Dialog>
              
      
           </div>
            
        );
    }
}
 
export default Products;