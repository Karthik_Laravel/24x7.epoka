import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';



class ImportExport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleExport = this.handleExport.bind(this);
    }

    handleSubmit(e)
    {

        let files=(e.target.files);

        let reader = new FileReader();

        reader.readAsDataURL(files[0]);

        reader.onload=(e) =>
        {
            const data = {file:e.target.result}

            fetch("http://localhost:8000/api/products/import",
            {
            method:"post",
            headers:{
                "content-type":"application/json"
            },
            body:JSON.stringify(data)
            })
            .then((response) =>response.json())
            .then((res) => console.log("testing"))
        }
        // window.location.reload()
    }

    handleExport()
    {
      console.log("handleExport");
      window.location.href="http://localhost:8000/api/user/list/export"
    }

    render() { 

        return (
          <div className="" >
            <input
              accept=".csv,text/csv"
              className=""
              style={{display:"none"}}
              id="contained-button-file"
              multiple
              type="file"
              onChange={e => this.handleSubmit(e)}
            />
            <label htmlFor="contained-button-file" style={{ margin:"0 5px" }}>
              <Button variant="contained" color="primary" component="span">
                IMPORT DATA
              </Button>
             
            </label>
            <Button style={{ margin:"0 5px" }} variant="contained" color="primary" onClick={this.handleExport} component="span">
                  EXPORT DATA
            </Button>
            
           
          </div>
        );
    }
}
 
export default ImportExport;

