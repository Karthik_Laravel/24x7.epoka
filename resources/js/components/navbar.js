import React from 'react';
import ReactDOM from 'react-dom';
import {Route,Link, BrowserRouter as Router} from 'react-router-dom';
import Home from './pages/home'
import User from './pages/User';
import Test from './pages/Test';
import Manage from './pages/UsersManage';


class App extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        sidebarOpen: true
      };
      this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }
   
    onSetSidebarOpen(open) {
      this.setState({ sidebarOpen: open });
    }
   
    render() {
      return (
        <div>
          <Router>

            <Route exact path="/" component={Home}></Route>
            <Route exact path="/user" component={User}></Route>
            <Route exact path="/manage-users" component={Manage}></Route>
            <Route exact path="/test" component={Test}></Route>
          </Router>
        </div>
      );
    }
  }
   
  export default App;