import React from 'react';
import ReactDOM from 'react-dom';
import {Route,Link, BrowserRouter as Router} from 'react-router-dom';
import Login from './components/login'
import Navbar from './components/navbar';
import Home from './components/pages/home';
import cookie from 'react-cookies'


export default class App extends React.Component{

    
    constructor(props)
    {
        super(props);

    this.state={
        loggedIn:true,
        loginButton:'',
        cookies:''
    }

    this.Isloggedin = this.Isloggedin.bind(this);
    
    
    }

    componentWillMount() {
        
        this.Isloggedin();

      }
    
    Isloggedin()
    {
        this.setState({loggedIn:cookie.load("loggedIn")});
    }

    checkLoggedin()
    {
        // this.setState({loginButton:<Navbar></Navbar>}) ;
        if (this.state.loggedIn) {
            this.setState({loginButton:<Navbar></Navbar>}) ;
        } 
        else {
            this.setState({loginButton:<Login></Login>})
        }
    }

    componentDidMount()
    {
        this.checkLoggedin();
    }

   
    render()
    {
        return (
            <div>
                    {this.state.loginButton}
            </div>
        );
    }
}



if (document.getElementById('example')) {
    ReactDOM.render(<App />, document.getElementById('example'));
}
