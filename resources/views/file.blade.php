@extends('layouts.app')
     <!-- Message -->
     @if(Session::has('message'))
        <p >{{ Session::get('message') }}</p>
     @endif

     <!-- Form -->
     <a href="/user/list/export" class="btn btn-primary">Export</a>
     <form method='post' action='/user/list/' enctype='multipart/form-data' >
       {{ csrf_field() }}
       <input type='file' name='file' >
       <input type='submit' name='submit' value='Import'>
     </form>
     <table border="1" width="100%">  
     <tr>
       <th>username</th>
       <th>email</th>
     </tr>
      @foreach ($data as $item)
          <tr>
            <td>{{ $item->username }}</td>
            <td>{{ $item->email }}</td>
          </tr>
      @endforeach


        <tr>
          <td colspan="2">
     {{ $data->links() }}
          </td>
        </tr>
    </table>
  </body>
</html>