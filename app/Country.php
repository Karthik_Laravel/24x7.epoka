<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class country extends Model
{
    protected $fillable = ["name","code"];

    public $table = "country";

    public $timestamps = false;
}
