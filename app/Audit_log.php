<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class audit_log extends Model
{
    protected $fillable = ["cust_id","user_id","DateUpdated","Ref","Model","Serial_Num","Order_Type","audit_status","Service_Level","Ship_FSL","Notes","Inv_Quantity","Inv_Partdesc","Inv_ProdClass"];

    public $table ="audit_log";

    public $timestamps = false;
}
