<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class audit_status extends Model
{
    protected $fillable=["Name","Code"]; 

    public $table = "audit_status";

    public $timestamps = false;
}
