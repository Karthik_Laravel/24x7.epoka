<?php
   
namespace App\Imports;
   
use App\User;
use App\Products;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Validators\ValidationException;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Support\Facades\DB;
    
class CsvImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public static function beforeImport(BeforeImport $event)
    {
        $worksheet = $event->reader->getActiveSheet();
        $highestRow = $worksheet->getHighestRow(); // e.g. 10

        if ($highestRow < 2) {
            $error = \Illuminate\Validation\ValidationException::withMessages([]);
            $failure = new Failure(1, 'rows', [0 => 'Now enough rows!']);
            $failures = [0 => $failure];
            throw new ValidationException($error, $failures);
        }
    }
    
    public function model(array $row)
    {
        // "cust_id","Sup_Location","Address","MFG","Model","Serial_Num","SLA","Part_Location","audit_status","price","from_date","to_date","status"
        
        // print_r($row);

        print_r($row["cust_id"]);

        die();

        return new Products([
            'cust_id' => $row['cust_id'],
            'Sup_Location' => $row['sup_location'],
            'Address' => $row['address'],
            'MFG' => $row['mfg'],
            'Model' => $row['model'],
            'Serial_Num' => $row['serial_num'],
            'SLA' => $row['sla'],
            'Part_Location' => $row['part_location'],
            'audit_status' => $row['audit_status'],
            'price' => $row['price'],
            'from_date' => $row['from_date'],
            'to_date' => $row['to_date'],
            'status' => $row['status']
        ]);
    }
}