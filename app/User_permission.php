<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_permission extends Model
{
    protected $fillable = ["roles_id","key"];

    public $table = "user_permission";

    public $timestamps = false;
}
