<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;
use App\Products;
use Illuminate\Support\Facades\DB;
use App\Imports\CsvImport;
use App\Exports\CsvExport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::table('products')->paginate(15);
    }

    public function country()
    {
        return DB::table('country')->select("Name")->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=array(
            "name"=>"Karthik",
            "subject" => "I am sending email via laravel"
        );

        // return view("template")->with("data",$data);

        Mail::to("veloukarthik@gmail.com")->send(new OrderShipped($data));

        return "Mail sent Successfully";
    }

    public function send(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ["cust_id","Sup_Location","Address","MFG","Model","Serial_Num","SLA","Part_Location","audit_status","price","from_date","to_date","status"];
        // $validatedData = $request->validate([
        //     'cust_id' => 'required|unique:posts|max:255',
        //     'Sup_Location' => 'required',
        //     ''=>'required',
        // ]);
        $products=Products::create($request->input());

        $success["message"]="Successfully Create the Product";

        return response()->json(['success'=>$success], 200); 

    }


    public function Import(Request $request)
    {
        // return $request->input('file');
        //    $img = preg_replace('#^data:image/[^;]+;base64,#', '', $request->input('file'));
        // $img = str_replace(' ','+',$img);
        $request->validate([
            'file' => 'required'
        ]);

        $file = str_replace("data:application/vnd.ms-excel;base64,","",$request->input('file'));

        $file = str_replace(' ','+',$file);

        $time = time();

        $files= storage_path()."/uploads/".$time.".csv";
        
        echo  file_put_contents($files,base64_decode($file));

        Excel::import(new CsvImport,$files);

        $success["message"]="Successfully Products are Imported";

        return response()->json(['success'=>$success], 200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $products= Products::findorfail($id);

       return $products;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products=Products::where("id",$id)->update($request->input());

        $success["message"]="Successfully Update the Product";

        return response()->json(['success'=>$success], 200); 



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products=Products::findorfail($id);

        $products->destroy($id);

        $success["message"]="Successfully Delete the Product";

        return response()->json(['success'=>$success], 201); 
    }
}
