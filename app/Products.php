<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = ["cust_id","Sup_Location","Address","MFG","Model","Serial_Num","SLA","Part_Location","audit_status","price","from_date","to_date","status"];

    public $table = "products";

    public $timestamps = false;
}
