<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ["name"];

    public $table ="roles";

    public $timestamps = false;
}
